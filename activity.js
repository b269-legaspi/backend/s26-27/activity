// Users
// ● First Name
// ● Last Name
// ● Email
// ● Password
// ● Is Admin?
// ● Mobile Number

// Orders
// ● User Id
// ● Transaction Date
// ● Status
// ● Total

// Products
// ● Name
// ● Description
// ● Price
// ● Stocks
// ● Is Active?
// ● SKU

// Order Products
// ● Order ID
// ● Product ID
// ● Quantity
// ● Price
// ● Sub Total

// Users
{
  "firstName": "Jeff",
  "lastName": "Legaspi",
  "email": "johnjeff.legaspi@gmail.com",
  "password": "password123",
  "isAdmin": false,
  "mobileNumber": "09981234567"
}

// Orders
{
  "userId": "00120",
  "transactionDate": "2021-01-01T12:00:00Z",
  "status": "success",
  "total": 12
}

// Products
{
  "name": "Century Tuna 420g",
  "description": "Canned Goods",
  "price": "200",
  "stocks": 42,
  "isActive": true,
  "SKU":"XYZ12345"
}

// Order Products
{
  "orderId" : "3947571",
  "productId" : "CG-02",
  "quantity" : 12,
  "price" : 95,
  "subTotal" : 1140
}